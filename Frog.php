<?php

require_once("Animal.php");

class Frog extends Animal
{
    public $name;
    public $legs = 4;
    public function jump()
    {
        echo "hop hop";
    }
}

?>