<?php
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    
    $sheep = new Animal("shaun");
    
    echo "Nama Hewan : $sheep->name <br>";
    echo "Jumlah kaki : $sheep->legs <br>";
    echo "Suhu tubuh darah dingin : $sheep->cold_blooded <br>";
    echo "<br>";

    $kodok = new Frog("buduk");
    echo "Nama Hewan : $kodok->name <br>";
    echo $kodok->Jump();
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Nama Hewan : $sungokong->name <br>";
    echo $sungokong->Yell();
    echo "<br>";
?>